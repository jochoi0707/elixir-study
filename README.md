## Elixir Meetup Notes

* Tool of the month: `sage`
* Research `UDP`, `Quick Protocol` from Google
* In `Go`, the way you model complex data structures is with `structs`
* In Elixir, using if/else and ternary statements is not usually good
* Use `mix format` for formatting to community standard

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `cards` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:cards, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/cards](https://hexdocs.pm/cards).

