defmodule Cards do
  @moduledoc """
    Provides methods for creating and handling a deck of cards
  """

  @doc """
    Returns a list of strings representing a deck of cards
  """
  def create_deck do
    values = ["Ace", "Two", "Three", "Four"]
    suits = ["Hearts", "Diamonds", "Clubs", "Spades"]

    for suit <- suits, value <- values do
      "#{value} of #{suit}"
    end
  end

  def shuffle_deck(deck) do
    Enum.shuffle(deck)
  end

  def contains?(deck, card) do
    Enum.member?(deck, card)
  end

  @doc """
    Generates a `hand` for a player

  ## Examples

      iex> deck = Cards.create_deck
      iex> hand = Cards.deal(deck, 1)
      iex> hand
      ["Ace of Hearts"]

  """
  def deal(deck, hand_size) do
    {hand, _rest} = Enum.split(deck, hand_size)
    hand
  end

  @doc """
    Checks whether or not your score is better than the class average

  ## Examples
      iex> class_points = [1,2,3,4,5]
      iex> your_points = 4
      iex> Cards.better_than_average(class_points, your_points)
      true
  """
  def better_than_average(class_points, your_points) do
    your_points > Enum.sum(class_points) / Enum.count(class_points)
  end

  @doc """
    Plays a game of rock, paper, scissors

  ## Examples
      iex> Cards.rps("paper", "paper")
      "Draw!"
  """
  def rps(p1, p2) do
    pone = "Player 1 won!"
    ptwo = "Player 2 won!"
    draw = "Draw!"

    case {p1, p2} do
      {"paper", "paper"} -> draw
      {"paper", "rock"} -> pone
      {"paper", "scissors"} -> ptwo
      {"rock", "paper"} -> ptwo
      {"rock", "rock"} -> draw
      {"rock", "scissors"} -> pone
      {"scissors", "paper"} -> pone
      {"scissors", "rock"} -> ptwo
      {"scissors", "scissors"} -> draw
    end
  end

  @doc """
    Return Yes for true, and No for false

  ## Examples
      iex> Cards.boolToWord(true)
      "Yes"
      iex> Cards.boolToWord(false)
      "No"
  """
  def boolToWord(b) do
    if b, do: "Yes", else: "No"
  end

  @doc """
    Remove all exclamation marks from a string

  ## Examples
    iex> Cards.remove_exclamation_marks "Hi! Steve!"
    "Hi Steve"
  """
  def remove_exclamation_marks(s) do
    s |> String.replace("!", "")
  end

  @doc """
    Convert a integer string to a numeric integer

  ## Examples
      iex> Cards.string_to_number "1234"
      1234
      iex> Cards.string_to_number "-4321"
      -4321
  """
  def string_to_number(str) do
    {num, _rest} = Integer.parse(str)
    num
  end

  @doc """
    Sums all integers between 1 and n

  ## Examples
      iex> Cards.summation(3)
      6
  """
  def summation(n) do
    Enum.to_list(1..n) |> Enum.sum()
  end

  @doc """
    Finds all occurences of d in the squares of list of 0 to n

  ## Examples
      iex> Cards.nb_dig(25, 1)
      11
  """
  def nb_dig(n, d) do
    num_str =
      Enum.to_list(0..n)
      |> Enum.map(fn x -> Integer.to_string(x * x) end)
      |> Enum.reduce(fn x, acc -> x <> acc end)

    String.length(num_str) - String.length(String.replace(num_str, Integer.to_string(d), ""))
  end
end
